<?php
use App\Http\Controllers\BotManController;
use BotMan\BotMan\Messages\Attachments\Video;
use BotMan\BotMan\Messages\Attachments\Image;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;


use App\article;
use BotMan\Drivers\Telegram\TelegramDriver;
use App\TelegramUser;

$botman = resolve('botman');


$botman->hears('start', function($bot){
    $conv = new App\Conversations\OnboardingConversation;
    $bot->startConversation($conv);


});


$botman->hears('Wie heiße ich\?', function($bot) {
    $name = $bot->userStorage()->get('name');
    $bot->reply('Du heißt '.$name);
});


$botman->hears('information', function($bot) {
    $user = $bot->getUser();
    $bot->reply('Id: '. $user->getId());
    $bot->reply('Vorname: '. $user->getFirstName());
    $bot->reply('Nachname: '. $user->getLastName());
    $bot->reply('Username: '. $user->getUsername());
});



$botman->hears('Hi', function ($bot) {
    $bot->reply('Hello you :)');
  
});


$botman->hears("showall", function ($bot) {
    $articles = article::orderBy('order')
              ->get();
    $bot->reply("All of the articles, listed for you:");
    foreach($articles as $article){
        $bot->reply($article->content);
    }
});



$botman->hears("showcontent", function($bot){
    $articles = article::orderBy('order')
              ->get();
    $id = $bot->getUser()->getId();
    $db_user = TelegramUser::where('id', $id)->first();
    $depth = $db_user->depth;
    $bot->reply("Du hast die Tiefe ".$depth." ausgewählt. Darum zeige ich dir folgende Inhalte an:");
    foreach($articles as $article){
        if($article->depth <= $depth){
            $bot->reply($article->content);
        }
    }
});

$botman->hears("depth {number}", function ($bot, $number){
    $articles = article::orderBy('order')
              ->get();
    foreach($articles as $article){
        if ($article->depth <= $number) {
            $bot->reply($article->content);
        }
    }
});


$botman->hears("testdbquery", function($bot){

    $db_users = TelegramUser::all();
    $user = $db_users[0];
    $state = $user->article_state;
    $depth = $user->depth;
    $bot->reply("Dies ist ein Befehl zum Testen von Datenbankabfragen.");
    $bot->reply($db_users[0]->id);
    for($i = 1; $i < 7; $i++){
        $my_article = article::where('depth','<=', $depth)
                    ->where('order', $i)
                    ->orderBy('order')
                    ->get();


        foreach($my_article as $article){
            $bot->reply($article->content);
        }
    }
});


$botman->hears("img", function($bot){
    $attachment = new Image('https://res.cloudinary.com/sagacity/image/upload/c_crop,h_1000,w_1500,x_0,y_0/c_limit,dpr_auto,f_auto,fl_lossy,q_80,w_1200/Kitten_murder_Jeff_Merkley_2_copy_hdpoxd.jpg');
    $message = OutgoingMessage::create('There you go.')->withAttachment($attachment);

    $bot->reply($message);

});



$botman->hears("video", function($bot){
    $attachment = new Video('http://techslides.com/demos/sample-videos/small.mp4');
    $message = OutgoingMessage::create('There you go.')->withAttachment($attachment);

    $bot->reply($message);

});




$botman->hears("FV", function($bot){
    $attachment = new Video('https://kommtdraufan.info/content/001_friedrich_intro.mp4');   
    $message = OutgoingMessage::create('There you go.')->withAttachment($attachment);
    $bot->reply($message);

});


$botman->hears("FV2", function($bot){
    $attachment = new Video('https://kommtdraufan.info/content/004_friedrich_wie-war-die-schule.mov');   
    $message = OutgoingMessage::create('There you go.')->withAttachment($attachment);
    $bot->reply($message);

});


$botman->hears("imgF", function($bot){
    $attachment = new Image('https://kommtdraufan.info/content/friedrich_foto.jpg');
    $message = OutgoingMessage::create('There you go.')->withAttachment($attachment);
    $bot->reply($message);

});




$botman->hears('Text', function ($bot){
    $bot->reply("Ich bin ein <b>fetter</b> Text. <pre> </pre> Und <u>das</u> ist <i>die</i> neue Zeile.", ['parse_mode' => 'HTML']);
    
});




$botman->hears('Schick mir einen Link!', function ($bot) {
    $bot->reply('http://techslides.com/demos/sample-videos/small.mp4');
#    $bot->reply('https://de.wikipedia.org/wiki/SS-Sonderlager_Hinzert');                                                                                                                                            

});




#-----------------
