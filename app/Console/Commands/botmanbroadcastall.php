<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;



class botmanbroadcastall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'botman:broadcastall';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Currently sends all database content to the tinker shell.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()

    {


    }
}
