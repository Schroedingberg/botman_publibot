<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TelegramUser extends Model
{
    //
    //
    protected $fillable = [
        'id','name', 'depth', 'schedule', 'article_state'
    ];
}
